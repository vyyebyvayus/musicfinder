import {Schema, model, Types, Model} from 'mongoose'
import bcrypt from 'bcryptjs'
import {IUserDocument} from '../interfaces/IUserDocument'

export interface IUser extends IUserDocument {
    comparePassword(password:string):boolean;
}

export interface IUserModel extends Model<IUser> {
    hashPassword(password:string): string;
}

const userSchema:Schema = new Schema({
    email:{type:String, required:true, unique:true},
    password: {type:String, required:true},
    searches: [{type: Types.ObjectId, ref:'Search'}]
})

userSchema.method('comparePassword', function (password: string): boolean {
    if (bcrypt.compareSync(password, this.password)) return true;
    return false;
})

userSchema.static('hashPassword', (password: string): string => {
    return bcrypt.hashSync(password);
});

export const userModel:IUserModel = model<IUser,IUserModel>('userModel', userSchema)

export default userModel