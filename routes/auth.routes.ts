import {Router, Request, Response} from 'express'
import config from 'config'
import bcryptjs from 'bcryptjs'
import {check, validationResult} from 'express-validator'
import userModel, { IUser, IUserModel } from '../models/userModel'
import { IUserDocument } from '../interfaces/IUserDocument'
const jwt = require('jsonwebtoken')
import { Document, DocumentQuery, Model } from 'mongoose'

const router = Router()

// /api/auth/register
router.post(
    '/register',
    [
        check('email','Wrong email').isEmail(),
        check('password','Pasword should be more than 6 symbols')
        .isLength({min:6})
    ],
    async(req:Request, res:Response)=>{
    try {
        const errors = validationResult(req)

        if(!errors.isEmpty()){
            return res.status(400).json({
                errors: errors.array(),
                message: "Incorect registration data"
            })
        }

        const {email, password} = req.body

        const candidate:IUserDocument|null = await userModel.findOne({email})
        console.log(candidate);        

        if (candidate){
               return res.status(400).json({message:`This email already in use / this is candidate ${candidate}`})
        }
        
        const hashedPassword = await bcryptjs.hash(password, 10)
        const user = new userModel({ email, password:hashedPassword})

        await user.save()

        res.status(201).json({message:"Succsessful registration"})

    } catch (e) {
        res.status(500).json({message:"Something went wrong with registration"})        
    }
})

router.post(
    '/login',
    [
        check('email','Type correct email').normalizeEmail().isEmail(),
        check('password','Type your password').exists()
    ],

    async(req:Request, res:Response)=>{
    try {
        const errors = validationResult(req)

        if(!errors.isEmpty()){
            return res.status(400).json({
                errors: errors.array(),
                message: "Incorect login data"
            })
        }

        const {email, password} = req.body
        
        const user:IUserDocument|null = await userModel.findOne({email})
        
        if(!user){
            return res.status(400).json({message:"User not found"})
        }        

        const userObject = await user?.toObject()
        console.log(userObject)
        console.log(password)

        const isMatch:boolean = await bcryptjs.compareSync(password, userObject.password)
        
        console.log(isMatch)

        if(isMatch == false){
            return res.status(400).json({message:'Wrong password try login again'})
        }

        console.log(user.id);
        
        //что-то не работает здесь
        const token = jwt.sign(
            {userId: user.id},
            config.get('secretJWT'),
            {expiresIn:'1h'}
        )

        console.log(token)
        
        res.json({token, userId: user.id}) 

        } catch (e) {
            res.status(500).json({message:"Something went wrong with login"})       
        }
    })


module.exports = router