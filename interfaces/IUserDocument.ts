import {Document, Types} from 'mongoose'

export interface IUserDocument extends Document{
    email:string,
    password:string,
    searches?: Array<Types.ObjectId>
}