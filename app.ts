import express from 'express'
import mongoose from 'mongoose'
import config from 'config'
import LastfmAPI from 'lastfmapi'
const app = express() 

app.use(express.json({}))
//app.use(express.urlencoded({extended:true}))

app.use('/api/auth', require('./routes/auth.routes'))




const PORT = config.get('PORT') || 5000

const start = async() =>{
    try {
        await mongoose.connect(config.get('mongoUri'),
    {
        useNewUrlParser:true,
        useCreateIndex:true,
        useUnifiedTopology:true
    }  
    )
    app.listen(PORT, ()=>console.log(`App has been started on port ${PORT}...`))
    } catch (e) {
     console.log('Server error:', e.message)   
     process.exit(1)
    }
    

}

start()